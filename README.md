# Лабораторная работа 4

## Подготовка окружения


## Запуск верификации и планировки (в классе)

[Спецификация консенсуса](src/test/resources/task_simple.yaml)

[График](src/test/resources/task_simple_backward/graph.png)

То, почему вероятности находятся выше 0.95 для любого количества сообщений, объяснимо высокой вероятностью выдачи 1 организациями. То, почему они выстраиваются решёткой, объяснению не поддаётся.

[Использование планировщика](src/test/java/SimpleTest.java)

Для реализации getResponse пришлось вручную добавить в json спецификацию поле с вероятностями выдачи 1 каждой организацией и добавить логику обработки этого поля в планировщик (это возможно, т.к. пример учебный).

Логи планировщика для модели -1869916142 [до фикса](src/test/resources/runTest_-1869916142_before_fix.log), [после фикса](src/test/resources/runTest_-1869916142_after_fix.log). В первом случае консенсус не достигнут из-за бага в планировщике, во втором достигнут с третьей попытки.





---
# consensus-scheduler

The tool reads analyzation result that produced consensus-analyzer, and based on the scheduling strategy sends a
transaction for confirmation. As results file can contain multiple models, one needs to choose one for sending a
transaction. Scheduler strategy uses different models choice:

* model with a minimal number of expected messages
* model with a max probability of confirmation
* model specified by id
* model specified by probability/expected messages pair

A user shall implement SenderService interface. There shall be a logic for sending transaction, and scheduler
properties. Then, provider SenderService to SendingConfirmationService and choose the method to send based on scheduler
strategy. One can find examples in SendingConfirmationServiceImplTest.

As an output SendingConfirmationService writes a log with sending transaction history and returns a boolean value.
Return value indicates a success of a transaction confirmation.

TODO:

* timeout for sending: if the message was sent some number of times to the same org, then go further
* waiting time for backward transitions
* take the closest number to given probability message pair
* make an example with hyperledger